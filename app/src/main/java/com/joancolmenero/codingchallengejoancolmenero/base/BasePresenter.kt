package com.joancolmenero.codingchallengejoancolmenero.base

interface BasePresenter<in T : BaseView> {
    fun attachView(view: T)
    fun detachView()
}