package com.joancolmenero.codingchallengejoancolmenero.base

interface BaseView {
    fun showError(errorMessage: String)
    fun showProgressBar(isVisible: Boolean)
}
