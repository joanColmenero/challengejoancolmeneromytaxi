package com.joancolmenero.codingchallengejoancolmenero.di

import com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.di.MapModule
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.di.CarListModule
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.di.PoiApiModule
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.ui.MainActivity
import com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.ui.CarsOnMapActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(
        modules =
        [CarListModule::class,
        PoiApiModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(
        modules =
        [MapModule::class])
    abstract fun bindMapActivity(): CarsOnMapActivity
}