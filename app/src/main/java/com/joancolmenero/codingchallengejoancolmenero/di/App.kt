package com.joancolmenero.codingchallengejoancolmenero.di

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class App : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<DaggerApplication> {
        val component = DaggerApplicationComponent.builder()
            .application(this).build()
        component.inject(this)
        return component
    }
}