package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.di

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.PoiApiService
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter.CarListPresenter
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.repository.CarListRepository
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.repository.Repository
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.ui.CarListContract
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.usecases.GetListPoiUseCase
import dagger.Module
import dagger.Provides


@Module
class CarListModule {

    @Provides
    fun provideCarListPresenter(getListPoiUseCase: GetListPoiUseCase): CarListContract.Presenter {
        return CarListPresenter(getListPoiUseCase)
    }

    @Provides
    fun provideListPoiUseCase(carListRepository: Repository): GetListPoiUseCase {
        return GetListPoiUseCase(carListRepository)
    }

    @Provides
    fun provideCarListRepository(poiApiService: PoiApiService): Repository {
        return CarListRepository(poiApiService)
    }

}