package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.di

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.PoiApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
@Singleton
class PoiApiModule {

    @Provides
    @Named("base_url")
    internal fun provideBaseUrl(): String {
        return "https://fake-poi-api.mytaxi.com/"
    }

    @Provides
    fun provideClient(): OkHttpClient {
        return OkHttpClient().newBuilder().build()
    }

    @Provides
    fun provideRetrofit(@Named("base_url") baseUrl: String, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideApiService(retrofit: Retrofit): PoiApiService {
        return retrofit.create(PoiApiService::class.java)
    }
}