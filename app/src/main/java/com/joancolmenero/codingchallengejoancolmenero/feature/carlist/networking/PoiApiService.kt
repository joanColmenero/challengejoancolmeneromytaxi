package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model.PoiResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface PoiApiService {

    @GET("/")
    fun getCars(
        @Query("p1Lat") p1Lat: Double,
        @Query("p1Lon") p1Lon: Double,
        @Query("p2Lat") p2Lat: Double,
        @Query("p2Lon") p2Lon: Double
    ): Observable<PoiResponse>

}