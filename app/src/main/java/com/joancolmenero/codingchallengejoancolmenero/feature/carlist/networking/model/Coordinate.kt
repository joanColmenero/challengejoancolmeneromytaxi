package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model

import java.io.Serializable

data class Coordinate(
    val latitude: Double,
    val longitude: Double
) : Serializable