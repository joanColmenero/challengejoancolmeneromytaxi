package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model

enum class FleetType {
    TAXI, POOLING
}