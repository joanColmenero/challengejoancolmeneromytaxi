package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model

data class Poi(
    val id: Long,
    val coordinate: Coordinate,
    val fleetType: String,
    val heading: Double
)
