package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model

data class PoiResponse(
    val poiList: List<Poi>
)