package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.ui.CarListContract
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.usecases.GetListPoiUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CarListPresenter @Inject constructor(
    private var getListPoiUseCase: GetListPoiUseCase) : CarListContract.Presenter {

    private var mView: CarListContract.View? = null
    private lateinit var subscription: Disposable


    override fun loadResults() {
        subscription = getListPoiUseCase.execute()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                mView?.let {
                    it.showProgressBar(false)
                    it.showResults(result)

                }
            },
                { error ->
                    mView?.let {
                        it.showProgressBar(false)
                        it.showError(error.localizedMessage)
                    }
                })
    }

    override fun detachView() {
        this.mView = null
    }

    override fun rxJavaUnsuscribe() {
        subscription.dispose()
    }

    override fun attachView(view: CarListContract.View) {
        this.mView = view
        this.mView?.showProgressBar(true)
    }

}