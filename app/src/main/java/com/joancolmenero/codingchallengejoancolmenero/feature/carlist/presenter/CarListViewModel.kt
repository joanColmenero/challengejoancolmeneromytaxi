package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model.Coordinate
import java.io.Serializable

data class CarListViewModel(
    val id: Long,
    val fleetType: String,
    val coordinate: Coordinate
) : Serializable