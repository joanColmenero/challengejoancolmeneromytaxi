package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.repository

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.PoiApiService
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model.PoiResponse
import io.reactivex.Observable

class CarListRepository(poiApiService: PoiApiService) : Repository {

    private var mCarList: MutableList<PoiResponse> = arrayListOf()
    private var mPoiApiService: PoiApiService = poiApiService
    private var lastTimestamp: Long = System.currentTimeMillis()

    private fun isUpdated(): Boolean {
        return System.currentTimeMillis() - lastTimestamp < CACHE_LIFETIME
    }


    override fun getResultFromNetwork(): Observable<PoiResponse> {
        return mPoiApiService.getCars(
             Hamburg.P1_LAT,
             Hamburg.P1_LON,
             Hamburg.P2_LAT,
             Hamburg.P2_LON)
            .doOnNext { mCarList.add(it)}
    }

    override fun getResultFromCache(): Observable<PoiResponse> {
        return if (isUpdated() && mCarList.size > 0) {
            Observable.fromIterable(mCarList)
        } else {
            lastTimestamp = System.currentTimeMillis()
            mCarList.clear()
            Observable.empty()
        }
    }

    override fun getResultData(): Observable<PoiResponse> {
        return getResultFromCache().switchIfEmpty(getResultFromNetwork())
    }

    companion object {
        private const val CACHE_LIFETIME = (20 * 1000).toLong()
    }
}