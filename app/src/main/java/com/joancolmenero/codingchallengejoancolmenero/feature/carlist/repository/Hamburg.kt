package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.repository

class Hamburg {

    companion object {
        const val P1_LAT: Double = 53.694865
        const val P1_LON: Double = 9.757589
        const val P2_LAT: Double = 53.394655
        const val P2_LON: Double = 10.099891
    }

}
