package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.repository

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model.PoiResponse
import io.reactivex.Observable

interface Repository {
    fun getResultFromNetwork(): Observable<PoiResponse>
    fun getResultFromCache(): Observable<PoiResponse>
    fun getResultData(): Observable<PoiResponse>
}