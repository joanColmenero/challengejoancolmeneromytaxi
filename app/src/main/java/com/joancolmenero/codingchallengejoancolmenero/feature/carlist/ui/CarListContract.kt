package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.ui

import com.joancolmenero.codingchallengejoancolmenero.base.BasePresenter
import com.joancolmenero.codingchallengejoancolmenero.base.BaseView
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter.CarListViewModel

interface CarListContract {

    interface View : BaseView {
        fun showResults(carsList: List<CarListViewModel>)
    }

    interface Presenter : BasePresenter<View> {
        fun rxJavaUnsuscribe()
        fun loadResults()
    }
}