package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.ui

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.joancolmenero.codingchallengejoancolmenero.R
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter.CarListViewModel
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.ui.adapter.CarListAdapter
import com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.ui.CarsOnMapActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import java.io.Serializable
import javax.inject.Inject

class MainActivity : AppCompatActivity(), CarListContract.View {

    private var adapter: CarListAdapter? = null
    @Inject
    lateinit var presenter: CarListContract.Presenter
    private lateinit var gridLayoutManager: GridLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews() {
        gridLayoutManager = GridLayoutManager(this, 2)
    }

    override fun showProgressBar(isVisible: Boolean) {
        progressbar.visibility = if (isVisible) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    override fun showError(errorMessage: String) {
        Snackbar.make(rv_car_list, getString(R.string.error_on_load_recyclerview), Snackbar.LENGTH_SHORT).show()
    }

    private fun onButtonLocationClicked(carsList: List<CarListViewModel>, position: Int) {
        val goToMap = Intent(
            this@MainActivity,
            CarsOnMapActivity::class.java
        )
        val bundle = Bundle().also {
            it.putSerializable("carsList", carsList as Serializable)
            it.putInt("carSelected", position)
        }
        goToMap.putExtras(bundle)
        startActivity(goToMap)
    }

    override fun showResults(carsList: List<CarListViewModel>) {
        adapter = CarListAdapter(carsList)
        rv_car_list.adapter = adapter
        rv_car_list.layoutManager = gridLayoutManager
        adapter?.let {
            it.onItemClick = { carList, position ->
                onButtonLocationClicked(carList, position)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        presenter.attachView(this)
        presenter.loadResults()
    }

    override fun onStop() {
        super.onStop()
        presenter.rxJavaUnsuscribe()
        presenter.detachView()
    }


}
