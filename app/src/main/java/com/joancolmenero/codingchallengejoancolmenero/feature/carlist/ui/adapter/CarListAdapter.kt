package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.joancolmenero.codingchallengejoancolmenero.R
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter.CarListViewModel


class CarListAdapter(private var listVehicle: List<CarListViewModel>) :
    RecyclerView.Adapter<CarListViewHolder>() {

    lateinit var onItemClick: ((carsList: List<CarListViewModel>, position: Int) -> Unit)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarListViewHolder {
        return CarListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.car_item, parent, false))
    }

    override fun getItemCount() = listVehicle.size

    override fun onBindViewHolder(holder: CarListViewHolder, position: Int) {
        holder.bind(listVehicle, listVehicle[position], onItemClick)
    }

}