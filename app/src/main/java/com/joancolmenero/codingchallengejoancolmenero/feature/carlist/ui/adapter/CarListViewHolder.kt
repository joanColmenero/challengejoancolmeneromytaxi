package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.joancolmenero.codingchallengejoancolmenero.R
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model.FleetType
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter.CarListViewModel
import kotlinx.android.synthetic.main.car_item.view.*

class CarListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val carType: ImageView = itemView.carType
    private val carTypeText: TextView = itemView.carTypeText
    private val btnGoToMap: Button = itemView.btn_go_to_map

    fun bind(listVehicles: List<CarListViewModel>, listVehicleViewModel: CarListViewModel, listener: ((carsList: List<CarListViewModel>, position: Int) -> Unit)?) {
        carTypeText.text = listVehicleViewModel.fleetType
        when (listVehicleViewModel.fleetType) {
            FleetType.TAXI.name -> carType.setImageResource(R.mipmap.taxi_car)
            FleetType.POOLING.name -> carType.setImageResource(R.mipmap.pooling_car)
            else -> carType.setImageResource(R.mipmap.ic_launcher)
        }
        btnGoToMap.setOnClickListener {
            listener?.invoke(
                listVehicles,
                position
            )
        }
    }
}