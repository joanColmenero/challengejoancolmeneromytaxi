package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.usecases

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter.CarListViewModel
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.repository.Repository
import io.reactivex.Observable
import javax.inject.Inject


class GetListPoiUseCase @Inject constructor(var repository: Repository) {

    fun execute(): Observable<List<CarListViewModel>> {

        return repository.getResultData().map { poiResponse ->
            poiResponse.poiList.map { poi ->
                CarListViewModel(
                    poi.id,
                    poi.fleetType,
                    poi.coordinate
                )
            }
        }
    }
}