package com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter.CarListViewModel

interface CarsOnMapContract {

    interface Presenter {
        fun onMapReady()
        fun setView(view: CarsOnMapContract.View)
        fun dettachView()
        fun setListOfCars(cars: List<CarListViewModel>)
        fun setPositionOfCardSelected(position: Int)
    }

    interface View {
        fun updateLocationOnMap(latitude: Double, longitude: Double, id: String)
        fun fillMarkersOnMap(cars: List<CarListViewModel>)
        fun clearMarkers()
    }
}