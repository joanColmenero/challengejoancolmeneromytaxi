package com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.di

import com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.CarsOnMapContract
import com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.presenter.CarsOnMapPresenter
import dagger.Module
import dagger.Provides

@Module
class MapModule {

    @Provides
    fun provideMapPresenter(): CarsOnMapContract.Presenter {
        return CarsOnMapPresenter(emptyList(), -1)
    }

}

