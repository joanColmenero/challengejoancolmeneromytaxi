package com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.presenter

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter.CarListViewModel
import com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.CarsOnMapContract

class CarsOnMapPresenter(
    private var vehicleList: List<CarListViewModel>,
    private var position: Int
) : CarsOnMapContract.Presenter {
    private var mView: CarsOnMapContract.View? = null

    override fun setListOfCars(cars: List<CarListViewModel>) {
        this.vehicleList = cars
    }

    override fun setPositionOfCardSelected(position: Int) {
        this.position = position
    }

    override fun setView(view: CarsOnMapContract.View) {
        mView = view

    }

    override fun onMapReady() {
        mView?.let {
            it.fillMarkersOnMap(vehicleList)
            it.updateLocationOnMap(
                vehicleList[position].coordinate.latitude,
                vehicleList[position].coordinate.longitude,
                vehicleList[position].id.toString()
            )
        }
    }

    override fun dettachView() {
        mView = null
    }
}