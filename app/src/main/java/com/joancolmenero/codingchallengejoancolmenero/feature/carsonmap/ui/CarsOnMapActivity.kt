package com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.joancolmenero.codingchallengejoancolmenero.R
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model.FleetType
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter.CarListViewModel
import com.joancolmenero.codingchallengejoancolmenero.feature.carsonmap.CarsOnMapContract
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_cars_on_map.*
import javax.inject.Inject


class CarsOnMapActivity : AppCompatActivity(), OnMapReadyCallback, CarsOnMapContract.View {

    private var mMap: GoogleMap? = null
    @Inject
    lateinit var mapPresenter: CarsOnMapContract.Presenter

    override fun updateLocationOnMap(latitude: Double, longitude: Double, id: String) {
        val carPosition = LatLng(latitude, longitude)
        val cameraPosition = CameraPosition.Builder()
            .target(carPosition)
            .zoom(12f).build()
        mMap?.let {
            it.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }

    override fun clearMarkers() {
        mMap?.let {
            it.clear()
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        mapPresenter.onMapReady()

    }

    override fun fillMarkersOnMap(cars: List<CarListViewModel>) {
        cars.forEach {
            mMap?.addMarker(
                MarkerOptions().position(LatLng(it.coordinate.latitude, it.coordinate.longitude))
                    .title(it.id.toString())
                    .snippet(it.fleetType)
                    .icon(BitmapDescriptorFactory.fromResource(getImageForMarker(it.fleetType)))
            )

        }
    }

    private fun getImageForMarker(carType: String): Int {
        return if (carType == FleetType.TAXI.name) R.mipmap.taxi_car else R.mipmap.pooling_car
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cars_on_map)
        setSupportActionBar(toolbar)

        mapPresenter.setListOfCars(intent.getSerializableExtra("carsList") as List<CarListViewModel>)
        mapPresenter.setPositionOfCardSelected(intent.getIntExtra("carSelected", -1))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        generateMap()
    }

    private fun generateMap() {

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    override fun onResume() {
        super.onResume()
        mapPresenter.setView(this)
    }

    override fun onStop() {
        super.onStop()
        mapPresenter.dettachView()
    }

}
