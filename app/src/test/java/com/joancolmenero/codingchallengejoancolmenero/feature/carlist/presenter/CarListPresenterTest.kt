package com.joancolmenero.codingchallengejoancolmenero.feature.carlist.presenter

import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.networking.model.Coordinate
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.ui.CarListContract
import com.joancolmenero.codingchallengejoancolmenero.feature.carlist.usecases.GetListPoiUseCase
import com.joancolmenero.codingchallengejoancolmenero.utils.RxImmediateSchedulerRule
import io.reactivex.Observable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class CarListPresenterTest {

    @get:Rule
    val schedulers = RxImmediateSchedulerRule()

    @Mock
    private lateinit var view : CarListContract.View
    @Mock
    private lateinit var getListPoiUseCase: GetListPoiUseCase
    private lateinit var presenter : CarListPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = CarListPresenter(getListPoiUseCase)
    }

    @Test
    fun loadResults() {

        //Assign
        presenter.attachView(view)
        Mockito.`when`(getListPoiUseCase.execute()).thenReturn(Observable.just(carList))

        //Act
        presenter.loadResults()

        //Assert
        Mockito.verify(view).showResults(carList)

    }


    companion object {

        private val carList = arrayListOf(
            CarListViewModel(1, "TAXI",Coordinate(10.00302,9.320320)),
            CarListViewModel(2, "POOLING", Coordinate(12.00302,12.320320))
        )
    }
}